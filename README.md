# Kafka Module

This is Library for Kafka producer and consumer based on spring-kafka.

## Setup Dependency

```xml
<dependency>
    <groupId>com.gitlab.residwi</groupId>
    <artifactId>spring-library-kafka</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/12070658/-/packages/maven</url>
    </repository>
</repositories>
```

## Kafka Producer

When we use spring-kafka, we can use `KafkaTemplate` to publish data to kafka, but in kafka Library, we
use `KafkaProducer` class to publish data to kafka.

```java
public class Service {

    @Autowired
    private KafkaProducer kafkaProducer;

    // publish data to kafka 
    SendResult<String, String> result = kafkaProducer.send(topic, key, payload);
    SendResult<String, String> result = kafkaProducer.send(producerEvent);
}
```

## Kafka Repository

Kafka Library also create Repository Pattern for send data to kafka. Sometimes this is useful to simplify send domain
data to kafka.

First, we need to create domain class for kafka object. It's simple POJO class, and to set kafka topic name, we can
use `@KafkaTopic`, and to set kafka key, we can use `@KafkaKey`

```java
@KafkaTopic("customer_topic")
public class CustomerEvent {

    @KafkaKey
    private String id;

    private Gender gender;

    private String firstName;

    private String lastName;

    private String email;
}
```

To create kafka repository. Kafka Library already create abstract class and interface to simplify this process.

```java

@Component
public class CustomerKafkaRepository extends AbstractKafkaRepository<CustomerEvent> implements KafkaRepository<CustomerEvent> {

}
``` 

Now we can send data to kafka using this repository object.

```java
public class Service {

    @Autowired
    private CustomerKafkaRepository customerKafkaRepository;

    SendResult<String, String> result = customerKafkaRepository.send(customerEvent);
}
```

## Kafka Producer Interceptor

Sometimes we want to do something before we send data to kafka. We can do it manually on our code. Or Kafka Library
already create `KafkaProducerInterceptor`. We can create bean class of `KafkaProducerInterceptor`, and register it to
spring.

```java
public interface KafkaProducerInterceptor {

    // this method will invoked before send data to kafka
    default ProducerEvent beforeSend(ProducerEvent event) {
        return event;
    }
}
```

## Kafka Consumer Interceptor

We can also add interceptor for kafka consumer using `KafkaConsumerInterceptor`.

```java
public interface KafkaConsumerInterceptor {
    
    // invoked before kafka listener invoked. If return true, kafka library will stop process
    default boolean beforeConsume(ConsumerRecord<String, String> consumerRecord) {
        return false;
    }

    // invoked after kafka listener success consumed data
    default void afterSuccessConsume(ConsumerRecord<String, String> consumerRecord) {

    }

    // invoked only if kafka listener failed consumed data and throw an exception
    default void afterFailedConsume(ConsumerRecord<String, String> consumerRecord, Throwable throwable) {

    }

}
```

## Log Kafka Message

Kafka Library already has `LogKafkaProducerInterceptor` and `LogKafkaConsumerInterceptor`. These interceptors are for log
payload. By default, Kafka Library will not log any payload in producer and consumer interceptor. To enable log interceptor, set to `true`.

```properties
library.kafka.logging.before-send=true
library.kafka.logging.before-consume=true
library.kafka.logging.after-success-consume=true
library.kafka.logging.after-failed-consume=true
```