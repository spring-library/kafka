package com.gitlab.residwi.spring.library.kafka.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.annotation.KafkaKey;
import com.gitlab.residwi.spring.library.kafka.annotation.KafkaTopic;
import com.gitlab.residwi.spring.library.kafka.producer.helper.KafkaHelper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = KafkaRepositoryTest.Application.class)
@EmbeddedKafka(
        topics = KafkaRepositoryTest.TOPIC
)
@DirtiesContext
class KafkaRepositoryTest {

    public static final String TOPIC = "KAFKA_REPOSITORY_TEST";

    @Autowired
    private EmbeddedKafkaBroker broker;

    private Consumer<String, String> consumer;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductKafkaRepository productKafkaRepository;

    @Autowired
    private CustomerKafkaRepository customerKafkaRepository;

    @BeforeEach
    void setUp() {
        consumer = KafkaHelper.newConsumer(broker);
        broker.consumeFromEmbeddedTopics(consumer, TOPIC);
    }

    @AfterEach
    void tearDown() {
        consumer.close();
    }

    @Test
    void testProductSuccess() throws JsonProcessingException {
        productKafkaRepository.send(new Product("id", "name"));
        ConsumerRecord<String, String> record = KafkaTestUtils.getSingleRecord(consumer, TOPIC);

        assertEquals("id", record.key());
        Product product = objectMapper.readValue(record.value(), Product.class);
        assertEquals("id", product.getId());
        assertEquals("name", product.getName());

        productKafkaRepository.send(new Product("id", "name"));
        record = KafkaTestUtils.getSingleRecord(consumer, TOPIC);

        assertEquals("id", record.key());
        product = objectMapper.readValue(record.value(), Product.class);
        assertEquals("id", product.getId());
        assertEquals("name", product.getName());
    }

    @Test
    void testCustomerSuccess() throws JsonProcessingException {
        customerKafkaRepository.send(new Customer("id", "name"));
        ConsumerRecord<String, String> record = KafkaTestUtils.getSingleRecord(consumer, TOPIC);

        assertEquals("id", record.key());
        Customer customer = objectMapper.readValue(record.value(), Customer.class);
        assertEquals("id", customer.getId());
        assertEquals("name", customer.getName());
    }

    @SpringBootApplication
    public static class Application {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

        @Bean
        public CustomerKafkaRepository customerKafkaRepository() {
            return new CustomerKafkaRepository();
        }

        @Bean
        public ProductKafkaRepository productKafkaRepository() {
            return new ProductKafkaRepository();
        }

    }

    public static class CustomerKafkaRepository extends AbstractKafkaRepository<Customer> implements KafkaRepository<Customer> {

    }

    public static class ProductKafkaRepository extends AbstractKafkaRepository<Product> implements KafkaRepository<Product> {

    }

    @KafkaTopic(KafkaRepositoryTest.TOPIC)
    public static class Customer {

        @KafkaKey
        private String id;

        private String name;

        public Customer() {
        }

        public Customer(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @KafkaTopic(KafkaRepositoryTest.TOPIC)
    public static class Product {

        @KafkaKey
        private String id;

        private String name;

        public Product() {
        }

        public Product(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
