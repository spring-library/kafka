package com.gitlab.residwi.spring.library.kafka.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.model.ProducerEvent;
import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import com.gitlab.residwi.spring.library.kafka.producer.helper.KafkaHelper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = KafkaProducerInterceptorTest.Application.class)
@EmbeddedKafka(
        topics = KafkaProducerInterceptorTest.TOPIC
)
@DirtiesContext
class KafkaProducerInterceptorTest {

    public static final String TOPIC = "KafkaProducerInterceptorTest";

    @Autowired
    private EmbeddedKafkaBroker broker;

    @Autowired
    private KafkaProducer kafkaProducer;

    private Consumer<String, String> consumer;

    @Autowired
    private ErrorFlag errorFlag;

    @BeforeEach
    void setUp() {
        consumer = KafkaHelper.newConsumer(broker);
        broker.consumeFromEmbeddedTopics(consumer, TOPIC);
    }

    @AfterEach
    void tearDown() {
        consumer.close();
    }

    @Test
    void testInterceptor() {
        errorFlag.setError(false);
        kafkaProducer.send(TOPIC, "key", "value");

        ConsumerRecord<String, String> record = KafkaTestUtils.getSingleRecord(consumer, TOPIC);

        assertEquals("key", record.key());
        assertEquals("value changed", record.value());
    }

    @Test
    void testInterceptorError() {
        errorFlag.setError(true);

        assertThrows(RuntimeException.class, () -> kafkaProducer.send(TOPIC, "key", "value"));
    }

    @SpringBootApplication
    public static class Application {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

        @Bean
        public ChangeBodyInterceptor changeBodyInterceptor(ErrorFlag errorFlag) {
            return new ChangeBodyInterceptor(errorFlag);
        }

        @Bean
        public ErrorFlag errorFlag() {
            return new ErrorFlag();
        }

    }

    public static class ChangeBodyInterceptor implements KafkaProducerInterceptor {

        private final ErrorFlag errorFlag;

        public ChangeBodyInterceptor(ErrorFlag errorFlag) {
            this.errorFlag = errorFlag;
        }

        @Override
        public ProducerEvent beforeSend(ProducerEvent event) {
            if (errorFlag.isError) {
                throw new RuntimeException("Ups error");
            } else {
                event.setValue(event.getValue() + " changed");
                return event;
            }
        }
    }

    public static class ErrorFlag {

        private boolean isError;

        public ErrorFlag() {
        }

        public ErrorFlag(boolean isError) {
            this.isError = isError;
        }

        public boolean isError() {
            return isError;
        }

        public void setError(boolean error) {
            isError = error;
        }
    }

}
