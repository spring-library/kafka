package com.gitlab.residwi.spring.library.kafka.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import com.gitlab.residwi.spring.library.kafka.producer.helper.KafkaHelper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest(classes = KafkaConsumerInterceptorTest.Application.class)
@EmbeddedKafka(
        topics = {KafkaConsumerInterceptorTest.TOPIC, KafkaConsumerInterceptorTest.TOPIC_GOODBYE}
)
@DirtiesContext
class KafkaConsumerInterceptorTest {

    public static final String TOPIC = "KafkaConsumerInterceptorTest";

    public static final String TOPIC_GOODBYE = "KafkaConsumerInterceptorTestGoodBye";

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private HelloListener helloListener;

    @Autowired
    private HelloInterceptor helloInterceptor;

    @Autowired
    private CounterInterceptor counterInterceptor;

    @Autowired
    private EmbeddedKafkaBroker broker;

    private Consumer<String, String> consumer;

    @BeforeEach
    void setUp() {
        consumer = KafkaHelper.newConsumer(broker);
        broker.consumeFromEmbeddedTopics(consumer, TOPIC);

        helloInterceptor.reset();
        counterInterceptor.reset();
    }

    @AfterEach
    void tearDown() {
        consumer.close();
    }

    @Test
    void testListener() throws InterruptedException {
        kafkaProducer.send(TOPIC, "key", "value");
        Thread.sleep(2_000L); // wait 2 seconds until message received by listener

        assertEquals("key", helloListener.key);
        assertEquals("value", helloListener.value);

        kafkaProducer.send(TOPIC, "key", "value");
        Thread.sleep(2_000L); // wait 2 seconds until message received by listener

        assertEquals("value", helloInterceptor.beforeConsume);
        assertEquals("value", helloInterceptor.afterSuccess);
        assertEquals(0, counterInterceptor.getBeforeConsume());
        assertEquals(0, counterInterceptor.getAfterSuccessConsume());
    }

    @Test
    void testInterceptorError() throws InterruptedException {
        kafkaProducer.send(TOPIC, "error", "value");
        Thread.sleep(2_000L); // wait 2 seconds until message received by listener

        assertEquals("value", helloInterceptor.beforeConsume);
        assertEquals("value", helloInterceptor.afterFailed);
        assertEquals(0, counterInterceptor.getBeforeConsume());
        assertEquals(0, counterInterceptor.getAfterSuccessConsume());
    }

    @Test
    void testInterceptorSkip() throws InterruptedException {
        kafkaProducer.send(TOPIC, "skip", "value");
        Thread.sleep(2_000L); // wait 2 seconds until message received by listener

        assertNotEquals("skip", helloListener.value);
        assertEquals(0, counterInterceptor.getBeforeConsume());
        assertEquals(0, counterInterceptor.getAfterSuccessConsume());
    }

    @Test
    void testInterceptorIsSupported() throws InterruptedException {
        kafkaProducer.send(TOPIC_GOODBYE, "key", "value");
        Thread.sleep(4_000L); // wait 4 seconds until message received by listener

        assertEquals(1, counterInterceptor.getBeforeConsume());
        assertEquals(1, counterInterceptor.getAfterSuccessConsume());
    }

    @SpringBootApplication
    public static class Application {


        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

        @Bean
        public HelloListener helloListener() {
            return new HelloListener();
        }

        @Bean
        public HelloInterceptor helloInterceptor() {
            return new HelloInterceptor();
        }

        @Bean
        public GoodByeListener goodByeListener() {
            return new GoodByeListener();
        }

        @Bean
        public CounterInterceptor counterInterceptor() {
            return new CounterInterceptor();
        }

    }

    public static class HelloListener {

        private String key;

        private String value;

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        @KafkaListener(topics = KafkaConsumerInterceptorTest.TOPIC, groupId = "custom-group")
        public void onMessage(ConsumerRecord<String, String> record) {
            if (record.key().equals("error")) {
                throw new RuntimeException("Error");
            } else {
                this.key = record.key();
                this.value = record.value();
            }
        }

    }

    public static class GoodByeListener {

        @KafkaListener(topics = KafkaConsumerInterceptorTest.TOPIC_GOODBYE, groupId = "goodbye-group")
        public void onMessage(ConsumerRecord<String, String> record) {
            // do nothing
        }

    }

    public static class HelloInterceptor implements KafkaConsumerInterceptor {

        private static final Logger log = LoggerFactory.getLogger(HelloInterceptor.class);

        private String beforeConsume;

        private String afterSuccess;

        private String afterFailed;

        public String getBeforeConsume() {
            return beforeConsume;
        }

        public String getAfterSuccess() {
            return afterSuccess;
        }

        public String getAfterFailed() {
            return afterFailed;
        }

        public void reset() {
            beforeConsume = null;
            afterSuccess = null;
            afterFailed = null;
        }

        @Override
        public boolean beforeConsume(ConsumerRecord<String, String> consumerRecord) {
            log.info("BEFORE");
            this.beforeConsume = consumerRecord.value();
            return consumerRecord.key().equals("skip");
        }

        @Override
        public void afterSuccessConsume(ConsumerRecord<String, String> consumerRecord) {
            log.info("AFTER");
            this.afterSuccess = consumerRecord.value();
        }

        @Override
        public void afterFailedConsume(ConsumerRecord<String, String> consumerRecord, Throwable throwable) {
            this.afterFailed = consumerRecord.value();
        }
    }

    public static class CounterInterceptor implements KafkaConsumerInterceptor {

        private Integer beforeConsume = 0;

        private Integer afterSuccessConsume = 0;

        private Integer afterFailedConsume = 0;

        public Integer getBeforeConsume() {
            return beforeConsume;
        }

        public Integer getAfterSuccessConsume() {
            return afterSuccessConsume;
        }

        public Integer getAfterFailedConsume() {
            return afterFailedConsume;
        }

        public void reset() {
            beforeConsume = 0;
            afterFailedConsume = 0;
            afterSuccessConsume = 0;
        }

        @Override
        public boolean isSupport(Object bean, Method method) {
            return bean.getClass().isAssignableFrom(GoodByeListener.class);
        }

        @Override
        public boolean beforeConsume(ConsumerRecord<String, String> consumerRecord) {
            beforeConsume++;
            return false;
        }

        @Override
        public void afterSuccessConsume(ConsumerRecord<String, String> consumerRecord) {
            afterSuccessConsume++;
        }

        @Override
        public void afterFailedConsume(ConsumerRecord<String, String> consumerRecord, Throwable throwable) {
            afterFailedConsume++;
        }
    }

}
