package com.gitlab.residwi.spring.library.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.producer.helper.KafkaHelper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes = KafkaProducerTest.Application.class)
@EmbeddedKafka(
        partitions = 1,
        topics = KafkaProducerTest.SAMPLE_TOPIC
)
@DirtiesContext
class KafkaProducerTest {

    public static final String SAMPLE_TOPIC = "SAMPLE_TOPIC";

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private EmbeddedKafkaBroker broker;

    private Consumer<String, String> consumer;

    @BeforeEach
    void setUp() {
        consumer = KafkaHelper.newConsumer(broker);
        broker.consumeFromEmbeddedTopics(consumer, SAMPLE_TOPIC);
    }

    @AfterEach
    void tearDown() {
        consumer.close();
    }

    @Test
    void testSendSuccess() {
        kafkaProducer.send(SAMPLE_TOPIC, "key", "kafka value");
        ConsumerRecord<String, String> record = KafkaTestUtils.getSingleRecord(consumer, SAMPLE_TOPIC);

        then(record.topic())
                .isEqualTo(SAMPLE_TOPIC);
        then(record.key())
                .isEqualTo("key");
        then(record.value())
                .isEqualTo("kafka value");
    }

    @Test
    void testSendError() {
        final Throwable throwable = catchThrowable(() -> kafkaProducer.send(null, null, null));

        then(throwable)
                .isInstanceOf(IllegalArgumentException.class);
    }

    @SpringBootApplication
    public static class Application {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

    }

}
