package com.gitlab.residwi.spring.library.kafka.repository;

import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import org.springframework.beans.factory.Aware;

public interface KafkaProducerAware extends Aware {

    void setKafkaProducer(KafkaProducer kafkaProducer);
}
