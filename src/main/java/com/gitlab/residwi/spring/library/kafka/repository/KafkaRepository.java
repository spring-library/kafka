package com.gitlab.residwi.spring.library.kafka.repository;

import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import org.springframework.kafka.support.SendResult;

public interface KafkaRepository<T> extends KafkaKeyAnnotationAware<T>, KafkaTopicAnnotationAware<T> {

    KafkaProducer getKafkaProducer();

    default SendResult<String, String> send(T data) {
        return send(getTopic(data), data);
    }

    default SendResult<String, String> send(String topic, T data) {
        return getKafkaProducer().send(topic, getKey(data), data);
    }

}
