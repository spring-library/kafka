package com.gitlab.residwi.spring.library.kafka.repository;

import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class KafkaProducerAwareBeanProcessor implements BeanPostProcessor {

    private final KafkaProducer kafkaProducer;

    public KafkaProducerAwareBeanProcessor(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof KafkaProducerAware) {
            var kafkaProducerAware = (KafkaProducerAware) bean;
            kafkaProducerAware.setKafkaProducer(kafkaProducer);
        }
        return bean;
    }
}
