package com.gitlab.residwi.spring.library.kafka.repository;

public interface KafkaKeyAnnotationAware<T> {

    default String getKey(T data) {
        try {
            return KafkaHelper.getKafkaKey(data);
        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("No annotation @KafkaKey on class %s", data.getClass().getName()));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
