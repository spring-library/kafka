package com.gitlab.residwi.spring.library.kafka.listener;

import com.gitlab.residwi.spring.library.kafka.interceptor.InterceptorUtils;
import com.gitlab.residwi.spring.library.kafka.interceptor.KafkaConsumerInterceptor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.kafka.listener.adapter.RecordMessagingMessageListenerAdapter;
import org.springframework.kafka.support.Acknowledgment;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

public class KafkaMessageListener extends RecordMessagingMessageListenerAdapter<String, String> {

    private final List<KafkaConsumerInterceptor> interceptors;

    public KafkaMessageListener(Object bean, Method method, KafkaListenerErrorHandler errorHandler, List<KafkaConsumerInterceptor> interceptors) {
        super(bean, method, errorHandler);
        this.interceptors = getSupportedKafkaConsumerInterceptors(bean, method, interceptors);
    }

    private List<KafkaConsumerInterceptor> getSupportedKafkaConsumerInterceptors(Object bean, Method method, List<KafkaConsumerInterceptor> interceptors) {
        return interceptors.stream()
                .filter(interceptor -> interceptor.isSupport(bean, method))
                .collect(Collectors.toList());
    }

    @Override
    public void onMessage(ConsumerRecord<String, String> record, Acknowledgment acknowledgment, Consumer<?, ?> consumer) {
        try {
            boolean skip = InterceptorUtils.fireBeforeConsume(record, interceptors);

            if (!skip) {
                super.onMessage(record, acknowledgment, consumer);
            }

            InterceptorUtils.fireAfterSuccessConsume(record, interceptors);
        } catch (Throwable throwable) {
            InterceptorUtils.fireAfterErrorConsume(record, throwable, interceptors);
            throw throwable;
        }
    }
}
