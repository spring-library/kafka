package com.gitlab.residwi.spring.library.kafka.interceptor;

import com.gitlab.residwi.spring.library.kafka.model.ProducerEvent;

public interface KafkaProducerInterceptor {

    default ProducerEvent beforeSend(ProducerEvent event) {
        return event;
    }
}
