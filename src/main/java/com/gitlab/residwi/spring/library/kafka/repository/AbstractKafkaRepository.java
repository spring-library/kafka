package com.gitlab.residwi.spring.library.kafka.repository;

import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;

public abstract class AbstractKafkaRepository<T> implements KafkaRepository<T>, KafkaProducerAware {

    private KafkaProducer kafkaProducer;

    @Override
    public KafkaProducer getKafkaProducer() {
        return kafkaProducer;
    }

    @Override
    public void setKafkaProducer(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }
}
