package com.gitlab.residwi.spring.library.kafka.model;

import org.apache.kafka.common.header.Headers;

public class ProducerEvent {

    private String topic;

    private Integer partition;

    private Headers headers;

    private String key;

    private Object value;

    private Long timestamp;

    public ProducerEvent() {
    }

    public ProducerEvent(String topic, Integer partition, Headers headers, String key, Object value, Long timestamp) {
        this.topic = topic;
        this.partition = partition;
        this.headers = headers;
        this.key = key;
        this.value = value;
        this.timestamp = timestamp;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProducerEvent event = (ProducerEvent) o;

        if (topic != null ? !topic.equals(event.topic) : event.topic != null) return false;
        if (partition != null ? !partition.equals(event.partition) : event.partition != null) return false;
        if (headers != null ? !headers.equals(event.headers) : event.headers != null) return false;
        if (key != null ? !key.equals(event.key) : event.key != null) return false;
        if (value != null ? !value.equals(event.value) : event.value != null) return false;
        return timestamp != null ? timestamp.equals(event.timestamp) : event.timestamp == null;
    }

    @Override
    public int hashCode() {
        int result = topic != null ? topic.hashCode() : 0;
        result = 31 * result + (partition != null ? partition.hashCode() : 0);
        result = 31 * result + (headers != null ? headers.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }
}
