package com.gitlab.residwi.spring.library.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.interceptor.InterceptorUtils;
import com.gitlab.residwi.spring.library.kafka.interceptor.log.LogKafkaConsumerInterceptor;
import com.gitlab.residwi.spring.library.kafka.interceptor.log.LogKafkaProducerInterceptor;
import com.gitlab.residwi.spring.library.kafka.listener.KafkaListenerBeanProcessor;
import com.gitlab.residwi.spring.library.kafka.producer.KafkaProducer;
import com.gitlab.residwi.spring.library.kafka.properties.KafkaProperties;
import com.gitlab.residwi.spring.library.kafka.repository.KafkaProducerAwareBeanProcessor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.KafkaListenerConfigUtils;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
@EnableConfigurationProperties({
        KafkaProperties.class
})
public class KafkaAutoConfiguration implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public KafkaProducer kafkaProducer(KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
        return new KafkaProducer(kafkaTemplate, objectMapper, InterceptorUtils.getProducerInterceptors(applicationContext));
    }

    @Bean
    public LogKafkaProducerInterceptor logKafkaProducerInterceptor(KafkaProperties kafkaProperties) {
        var interceptor = new LogKafkaProducerInterceptor();
        interceptor.setKafkaProperties(kafkaProperties);
        return interceptor;
    }

    @Bean
    public LogKafkaConsumerInterceptor logKafkaConsumerInterceptor(KafkaProperties kafkaProperties) {
        var interceptor = new LogKafkaConsumerInterceptor();
        interceptor.setKafkaProperties(kafkaProperties);
        return interceptor;
    }

    @Bean
    public KafkaProducerAwareBeanProcessor kafkaProducerAwareBeanProcessor(KafkaProducer kafkaProducer) {
        return new KafkaProducerAwareBeanProcessor(kafkaProducer);
    }

    @SuppressWarnings("rawtypes")
    @Bean(KafkaListenerConfigUtils.KAFKA_LISTENER_ANNOTATION_PROCESSOR_BEAN_NAME)
    public KafkaListenerBeanProcessor kafkaListenerBeanProcessor() {
        return new KafkaListenerBeanProcessor();
    }

    @Bean(KafkaListenerConfigUtils.KAFKA_LISTENER_ENDPOINT_REGISTRY_BEAN_NAME)
    public KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry() {
        return new KafkaListenerEndpointRegistry();
    }
}
