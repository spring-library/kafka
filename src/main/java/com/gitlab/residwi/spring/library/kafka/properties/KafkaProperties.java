package com.gitlab.residwi.spring.library.kafka.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("library.kafka")
public class KafkaProperties {

    private LoggingProperties logging = new LoggingProperties();

    public KafkaProperties(LoggingProperties logging) {
        this.logging = logging;
    }

    public KafkaProperties() {
    }

    public LoggingProperties getLogging() {
        return this.logging;
    }

    public void setLogging(LoggingProperties logging) {
        this.logging = logging;
    }

    public static class LoggingProperties {

        private boolean beforeSend = false;

        private boolean beforeConsume = false;

        private boolean afterSuccessConsume = false;

        private boolean afterFailedConsume = false;

        public LoggingProperties(boolean beforeSend, boolean beforeConsume, boolean afterSuccessConsume, boolean afterFailedConsume) {
            this.beforeSend = beforeSend;
            this.beforeConsume = beforeConsume;
            this.afterSuccessConsume = afterSuccessConsume;
            this.afterFailedConsume = afterFailedConsume;
        }

        public LoggingProperties() {
        }

        public boolean isBeforeSend() {
            return this.beforeSend;
        }

        public void setBeforeSend(boolean beforeSend) {
            this.beforeSend = beforeSend;
        }

        public boolean isBeforeConsume() {
            return this.beforeConsume;
        }

        public void setBeforeConsume(boolean beforeConsume) {
            this.beforeConsume = beforeConsume;
        }

        public boolean isAfterSuccessConsume() {
            return this.afterSuccessConsume;
        }

        public void setAfterSuccessConsume(boolean afterSuccessConsume) {
            this.afterSuccessConsume = afterSuccessConsume;
        }

        public boolean isAfterFailedConsume() {
            return this.afterFailedConsume;
        }

        public void setAfterFailedConsume(boolean afterFailedConsume) {
            this.afterFailedConsume = afterFailedConsume;
        }
    }
}
