package com.gitlab.residwi.spring.library.kafka.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.kafka.interceptor.InterceptorUtils;
import com.gitlab.residwi.spring.library.kafka.interceptor.KafkaProducerInterceptor;
import com.gitlab.residwi.spring.library.kafka.model.ProducerEvent;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;

import java.util.List;
import java.util.Objects;

public class KafkaProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final ObjectMapper objectMapper;

    private final List<KafkaProducerInterceptor> producerInterceptors;

    public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper, List<KafkaProducerInterceptor> producerInterceptors) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
        this.producerInterceptors = producerInterceptors;
    }

    public SendResult<String, String> send(String topic, String key, Object value) {
        var producerEvent = new ProducerEvent();
        producerEvent.setTopic(topic);
        producerEvent.setKey(key);
        producerEvent.setValue(value);
        return send(producerEvent);
    }

    public SendResult<String, String> send(ProducerEvent producerEvent) {
        var event = InterceptorUtils.fireBeforeSend(producerEvent, producerInterceptors);

        return sendWithKafkaTemplate(toProducerRecord(event));
    }

    private SendResult<String, String> sendWithKafkaTemplate(ProducerRecord<String, String> producerRecord) {
        try {
            return kafkaTemplate.send(producerRecord).get();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private ProducerRecord<String, String> toProducerRecord(ProducerEvent event) {
        return new ProducerRecord<>(
                event.getTopic(),
                event.getPartition(),
                event.getTimestamp(),
                event.getKey(),
                getValue(event),
                getHeaders(event)
        );
    }

    private Headers getHeaders(ProducerEvent event) {
        if (Objects.isNull(event.getHeaders())) {
            return new RecordHeaders();
        } else {
            return event.getHeaders();
        }
    }

    private String getValue(ProducerEvent event) {
        if (event.getValue() instanceof String) {
            return (String) event.getValue();
        } else {
            return toJson(event.getValue());
        }
    }

    private String toJson(Object value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
