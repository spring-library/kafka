package com.gitlab.residwi.spring.library.kafka.interceptor.log;

import com.gitlab.residwi.spring.library.kafka.interceptor.KafkaConsumerInterceptor;
import com.gitlab.residwi.spring.library.kafka.properties.KafkaProperties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;

public class LogKafkaConsumerInterceptor implements KafkaConsumerInterceptor, Ordered {

    private static final Logger log = LoggerFactory.getLogger(LogKafkaConsumerInterceptor.class);

    private KafkaProperties kafkaProperties;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public void setKafkaProperties(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Override
    public boolean beforeConsume(ConsumerRecord<String, String> consumerRecord) {
        if (kafkaProperties.getLogging().isBeforeConsume()) {
            log.info("Receive from topic {} with message {}:{}", consumerRecord.topic(), consumerRecord.key(), consumerRecord.value());
        }
        return false;
    }

    @Override
    public void afterSuccessConsume(ConsumerRecord<String, String> consumerRecord) {
        if (kafkaProperties.getLogging().isAfterSuccessConsume()) {
            log.info("Success consume from topic {} with message {}:{}", consumerRecord.topic(), consumerRecord.key(), consumerRecord.value());
        }
    }

    @Override
    public void afterFailedConsume(ConsumerRecord<String, String> consumerRecord, Throwable throwable) {
        if (kafkaProperties.getLogging().isAfterFailedConsume()) {
            log.error(String.format("Failed consume from topic %s with message %s:%s and exception %s", consumerRecord.topic(), consumerRecord.key(), consumerRecord.value(), throwable.getMessage()), throwable);
        }
    }
}
