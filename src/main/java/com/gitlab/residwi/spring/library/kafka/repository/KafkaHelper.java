package com.gitlab.residwi.spring.library.kafka.repository;

import com.gitlab.residwi.spring.library.kafka.annotation.KafkaKey;
import com.gitlab.residwi.spring.library.kafka.annotation.KafkaTopic;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class KafkaHelper {

    private static final Map<String, Field> fieldCache = new ConcurrentHashMap<>();

    private static final Map<String, String> topicCache = new ConcurrentHashMap<>();

    private KafkaHelper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static String getKafkaTopic(Class<?> aClass) {
        var name = aClass.getName();
        var topic = topicCache.get(name);

        if (topic != null) {
            return topic;
        }

        return topicCache.computeIfAbsent(name, s -> {
            var annotation = AnnotationUtils.findAnnotation(aClass, KafkaTopic.class);

            if (annotation != null) {
                return annotation.value();
            } else {
                return null;
            }
        });
    }

    public static String getKafkaKey(Object data) throws IllegalAccessException {
        var field = doFindField(data);
        if (field != null) {
            return (String) field.get(data);
        } else {
            return null;
        }
    }

    private static Field doFindField(Object data) {
        var name = data.getClass().getName();
        var field = fieldCache.get(name);

        if (field != null) {
            return field;
        }

        return fieldCache.computeIfAbsent(name, s -> {
            List<Field> fieldsListWithAnnotation = FieldUtils.getFieldsListWithAnnotation(data.getClass(), KafkaKey.class);
            if (!fieldsListWithAnnotation.isEmpty()) {
                var value = fieldsListWithAnnotation.get(0);
                value.setAccessible(true);
                return value;
            }
            return null;
        });
    }
}
