package com.gitlab.residwi.spring.library.kafka.repository;

public interface KafkaTopicAnnotationAware<T> {

    default String getTopic(T data) {
        try {
            return KafkaHelper.getKafkaTopic(data.getClass());
        } catch (NullPointerException e) {
            throw new NullPointerException(String.format("No annotation @KafkaTopic on class %s", data.getClass().getName()));
        }
    }
}
