package com.gitlab.residwi.spring.library.kafka.interceptor.log;

import com.gitlab.residwi.spring.library.kafka.interceptor.KafkaProducerInterceptor;
import com.gitlab.residwi.spring.library.kafka.model.ProducerEvent;
import com.gitlab.residwi.spring.library.kafka.properties.KafkaProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;

public class LogKafkaProducerInterceptor implements KafkaProducerInterceptor, Ordered {

    private static final Logger log = LoggerFactory.getLogger(LogKafkaProducerInterceptor.class);

    private KafkaProperties kafkaProperties;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public void setKafkaProperties(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Override
    public ProducerEvent beforeSend(ProducerEvent event) {
        if (kafkaProperties.getLogging().isBeforeSend()) {
            log.info("Send message to kafka : {}", event);
        }
        return event;
    }
}
